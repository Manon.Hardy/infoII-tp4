class File:
    def __init__(self):
        self.items = []

    def get_priority(self, item):
        return item[0]

    def enqueue(self, item):
        self.items.append(item)
        self.items.sort(key =self.get_priority)

    def dequeue(self):
        return self.items.pop(0)

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

file = File()
file.enqueue((4, "Nathan"))
file.enqueue((2, "Julia"))
file.enqueue((1, "Amandine"))
file.enqueue((3, "Mathias"))
print(file.dequeue())
print(file.dequeue())
print(file.dequeue())
print(file.dequeue())
